import React, { Component }  from 'react';
import ReactDOM from 'react-dom';
import '../scss/index.scss';

class App extends Component {
    render() {
        return (
            <div className="content">
                <h3 className="title">Technical stuff inside this page:</h3>
                <div className="sub-content">
                    <ul>
                        <li>Babel JSX bundler</li>
                        <li>ReactJS 'App.js & class App render'</li>
                        <li>SASS bundler file</li>
                        <li>Foundation Zurb Framework style, ex: typo, button</li>
                        <li>Compass SASS Framework, ex:<code>box-shadow</code>
                            effects (frame & button)</li>
                    </ul>
                </div>
                <h2 className="django-power">Django powered!</h2>
                <a href="about.html" className="mybutton">Learn More</a>
            </div>
        );
    }
}
export default App;
